package com.api.britishbulldogsserver.service;

import com.api.britishbulldogsserver.model.Game;
import com.api.britishbulldogsserver.model.Player;
import com.api.britishbulldogsserver.model.PlayerInfo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.util.*;

@Setter
@Getter
@Service
public class GameManager {

    private Map<Integer, Game> games;

    public GameManager() {
        // Create first instance of a game,  // Games list length plus one?
        Map<Integer, Game> games = new HashMap<>();
        games.put(0, new Game(0));
        this.games = games;
    }

    public Player joinGame(WebSocketSession session) {
        // Get first game that isn't full
        Game availableGame = new Game();
        for (Map.Entry<Integer, Game> entry : games.entrySet()) {
            if (entry.getValue().getPlayers().size() < 100) {
                availableGame = entry.getValue();
                break;
            }
        }

        // Create player
        Random random = new Random();
        int randomNumber = random.nextInt((1500 - 0) + 1) + 0;
        Integer playerId = availableGame.getPlayers().size() + 1;
        Player player = new Player(playerId, randomNumber, 25, availableGame, session);

        // Add player to game
        availableGame.getPlayers().put(playerId, new PlayerInfo(player));
        return player;
    }

}
