package com.api.britishbulldogsserver.service;

import com.api.britishbulldogsserver.model.PlayerInfo;
import com.api.britishbulldogsserver.model.message.Movement;
import org.springframework.stereotype.Service;

@Service
public class PlayerManager {

    private GameManager gameManager;

    public PlayerManager(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    public void handlePlayerMovement(Movement movement) {
        Integer gameId = movement.getGameId();
        Integer playerId = movement.getPlayerId();

        PlayerInfo playerInfo = gameManager.getGames().get(gameId).getPlayers().get(playerId);

        playerInfo.setY(movement.getY());
        playerInfo.setX(movement.getX());
    }

}
