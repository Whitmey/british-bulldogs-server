package com.api.britishbulldogsserver.service;

import com.api.britishbulldogsserver.model.message.IncomingMessage;
import com.api.britishbulldogsserver.model.message.Movement;
import com.api.britishbulldogsserver.util.Deserializer;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.BinaryMessage;

@Service
public class EventManager {

    private final GameManager gameManager;
    private final PlayerManager playerManager;
    private final Deserializer deserializer;

    public EventManager(GameManager gameManager) {
        this.gameManager = gameManager;
        this.playerManager = new PlayerManager(gameManager);
        this.deserializer = new Deserializer();
    }

    public void handleMessage(BinaryMessage binaryMessage) throws Exception {
        IncomingMessage message = deserializer.process(binaryMessage);

        switch (message.getType()) {
            case MOVEMENT:
                playerManager.handlePlayerMovement(new Movement(message.getData()));
                break;
            case CONNECTED:
                System.out.println("Connected");
                break;
        }

    }

}
