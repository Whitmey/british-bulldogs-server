package com.api.britishbulldogsserver.util;

import com.api.britishbulldogsserver.model.message.IncomingMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.socket.BinaryMessage;

public class Deserializer {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public Deserializer() {}

    public IncomingMessage process(BinaryMessage binaryMessage) throws Exception {
        return objectMapper.readValue(binaryMessage.getPayload().array(), IncomingMessage.class);
    }

}
