package com.api.britishbulldogsserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BritishBulldogsServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BritishBulldogsServerApplication.class, args);
	}

}
