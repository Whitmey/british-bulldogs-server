package com.api.britishbulldogsserver.websocket;

import com.api.britishbulldogsserver.model.message.Message;
import com.api.britishbulldogsserver.model.message.MessageType;
import com.api.britishbulldogsserver.model.Player;
import com.api.britishbulldogsserver.service.EventManager;
import com.api.britishbulldogsserver.service.GameManager;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import java.io.IOException;

@Component
public class MessageHandler extends BinaryWebSocketHandler {

    private GameManager gameManager;
    private EventManager eventManager;

    public MessageHandler(GameManager gameManager) {
        this.gameManager = gameManager;
        this.eventManager = new EventManager(gameManager);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        Player player = gameManager.joinGame(session);
        try {
            synchronized(session) {
                session.sendMessage(new Message(MessageType.CONNECTED, player).asBinary());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
        try {
            eventManager.handleMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        String playerId = session.getId();
//        int gameId = 0;
//        PlayerInfo playerInfo = gameManager.getGames().get(gameId).getPlayers().get(playerId);
//        playerInfo.setX(25);
//        playerInfo.setY(50);
        // if there is a collision, then verify.
        // Tip: interpolate data on client (make movement assumptions between message).
    }

//    @Override
//    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) {
//        String playerId = session.getId();
//        // find player in gameManager? Is going to be costly as won't know the gameId
//        // Remove player from gameState observer
//        // Remove player from in memory cache
//    }

}


// try {
//   session.sendMessage(new BinaryMessage("hello there".getBytes()));
// } catch (IOException e) {
//   e.printStackTrace();
// }