package com.api.britishbulldogsserver.model;

import com.api.britishbulldogsserver.model.message.Message;
import com.api.britishbulldogsserver.model.message.MessageType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

@Getter
@JsonIgnoreProperties(value = { "session" })
public class Player implements Observer {

    private Integer id;
    private int x;
    private int y;
    private Game game;
    private WebSocketSession session;

    public Player(Integer playerId, int x, int y, Game game, WebSocketSession session) {
        this.id = playerId;
        this.x = x;
        this.y = y;
        this.game = game;
        this.session = session;
        game.addObserver(this);
    }

    public void update(Observable o, Object arg) {
        try {
            synchronized(session) {
                session.sendMessage(new Message(MessageType.UPDATE_PLAYERS, arg).asBinary());
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

}
