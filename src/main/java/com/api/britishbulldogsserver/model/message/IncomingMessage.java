package com.api.britishbulldogsserver.model.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.LinkedHashMap;

@Getter
public class IncomingMessage {

    private MessageType type;
    private LinkedHashMap<String, String> data;

    @JsonCreator
    public IncomingMessage(@JsonProperty("type") MessageType type, @JsonProperty("data") LinkedHashMap<String, String> data) {
        this.type = type;
        this.data = data;
    }

}
