package com.api.britishbulldogsserver.model.message;

public enum MessageType {
    CONNECTED, UPDATE_PLAYERS, MOVEMENT
}