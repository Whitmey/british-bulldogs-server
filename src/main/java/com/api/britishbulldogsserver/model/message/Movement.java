package com.api.britishbulldogsserver.model.message;

import lombok.Getter;

import java.util.LinkedHashMap;

@Getter
public class Movement {

    private Integer playerId;
    private Integer gameId;
    private int x;
    private int y;

    public Movement(LinkedHashMap<String, String> map) {
        this.playerId = Integer.parseInt(map.get("id"));
        this.gameId = Integer.parseInt(map.get("gameId"));
        this.x = Integer.parseInt(map.get("x"));
        this.y = Integer.parseInt(map.get("y"));
    }

}
