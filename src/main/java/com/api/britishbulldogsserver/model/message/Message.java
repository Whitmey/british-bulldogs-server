package com.api.britishbulldogsserver.model.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import org.springframework.web.socket.BinaryMessage;

import java.io.IOException;

@Getter
public class Message {

    private static ObjectMapper objectMapper = new ObjectMapper();

    private MessageType type;
    private Object data;

    public Message(MessageType type, Object data) {
        this.type = type;
        this.data = data;
    }

    public BinaryMessage asBinary() throws IOException {
        return new BinaryMessage(objectMapper.writeValueAsBytes(this));
    }

}
