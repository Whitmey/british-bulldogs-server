package com.api.britishbulldogsserver.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerInfo {

    private int x;
    private int y;

    public PlayerInfo(Player player) {
        this.x = player.getX();
        this.y = player.getY();
    }

}
