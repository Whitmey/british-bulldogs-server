package com.api.britishbulldogsserver.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.*;

@Getter
@NoArgsConstructor
public class Game extends Observable {

    private Integer id;
    private Map<Integer, PlayerInfo> players;

    public Game(Integer id) {
        this.id = id;
        this.players = new HashMap<>();
        triggerEventLoop();
    }

    public void stateChanged() {
        setChanged();
        notifyObservers(getPlayers());
    }

    public void updateGameState() {
        stateChanged();
    }

    public void triggerEventLoop() {
        int delay = 16;   // delay for 5 sec.
        int period = 16;  // repeat every sec.
        new Timer().scheduleAtFixedRate(new TimerTask() {
            public void run() {
                updateGameState();
            }
        }, delay, period);
    }

}
